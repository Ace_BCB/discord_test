module.exports = {
    name: 'reload',
    description: 'reloads a command without shutting down the bot',
    usage: '[command name]',
    args: true,
    execute(msg, args) {
        //find command. If the nam wasn't recognized see if it was an alias
        const name = args[0].toLowerCase();
        const command = msg.client.commands.get(name) || msg.client.commands.find(c => c.aliases && c.aliases.includes(name));

        if (!command) {
            msg.reply('there is no command name or alias');
            return;
        }

        delete require.cache[require.resolve(`./${command.name}.js`)];

        try {
            const newCommand = require(`./${command.name}.js`);
            msg.client.commands.set(newCommand.name, newCommand);
            msg.channel.send('Reload succesful');
        } catch (error) {
            console.error(error);
            msg.channel.send(`There was an error trying to reload command \'${command.name}\': \'${error.message}\'`);
        }
    } 
}