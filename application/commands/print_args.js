module.exports = {
    name: 'print_args',
    description: 'print arguments"',
    usage: '<arg1> <arg2> ...',
    args: true,
    execute(msg, args) {
        msg.channel.send('arguments: ' + args);
    }
}