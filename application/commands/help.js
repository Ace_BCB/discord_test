const {prefix} = require('../config.json');

module.exports = {
    name: 'help',
    description: 'prints the commands and usage of them',
    usage: '[command name]',
    aliases: ['h', 'commands'],
    cooldown: 5,
    execute(msg, args) {
        const data = [];
        const {commands} = msg.client;
        
        //if there aren't any arguments in the help call send run this block
        if (!args.length) {
            data.push("Here is a list of all the commands:");
            data.push(commands.map(command => command.name).join(", "));
            data.push("\nYou can send " + prefix + "help [command name] to get info on a specific command.");

            msg.author.send(data, {split:true})
                .then(() => {
                    if (msg.channel.type === "dm") return;
                    msg.reply("I have sent you a DM with all the commands.");
                })
                .catch(error => {
                    console.error("Could not sond help DM to " + msg.author.tag, error);
                    msg.reply("There was an error trying to DM you.");
                });
            return;
        }

        //find command. If the nam wasn't recognized see if it was an alias
        const name = args[0].toLowerCase();
        const command = commands.get(name) || commands.find(c => c.aliases && c.aliases.includes(name));

        if (!command) {
            msg.reply('that was not a valid command');
            return;
        }

        data.push(`**Name:** ${command.name}`);

        //append the different attributes of a command to help message
        if (command.aliases) data.push(`**Aliases:** ${command.aliases.join(', ')}`);
        if (command.description) data.push(`**Description:** ${command.description}`);
        if (command.usage) data.push(`**Usage:** ${prefix}${command.name} ${command.usage}`);

        data.push(`**Cooldown:** ${command.cooldown || 3}s`);

        msg.channel.send(data, {split:true});
    }
}