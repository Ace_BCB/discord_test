const { description, aliases } = require("./help")

const ytdl = require('ytdl-core');

module.exports = {
    name: 'yt',
    description: 'Plays audio from a youtube video',
    usage: '<link>',
    aliases: ['music', 'play'],
    cooldown: 5,
    args: true,
    execute (msg, args) {
        let queue = [];
        queue.push(args[0]);
        const voiceChannel = msg.member.voice.channel;

        voiceChannel.join().then(connection => {
            play(connection, msg);
        })
    }
}

function play(connection, msg) {
    const stream = ytdl(queue[0], {filter: 'audioonly'});
    const dispatcher = connection.play(stream);
    queue.shift();
    
    dispatcher.on('finish', () => { 
        if (queue[0]) {
            play(connection, msg);        
        } else {
            voiceChannel.leave()
        }
    })
}
