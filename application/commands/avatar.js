
module.exports = {
    name: 'avatar',
    description: 'prints the avatar of all mentioned users',
    usage: '<@user1> <@user2> ...',
    args: true,
    execute(msg, args) {
        if (!msg.mentions.users.size) {
            msg.channel.send('Your avatar ' + msg.author.displayAvatarURL({format: "png", dynamic: true}));
            return;
        }

        const avatars = msg.mentions.users.map(user => {
            return user.username + ' avatar ' + user.displayAvatarURL({format: "png", dynamic:true});
        });

        msg.channel.send(avatars);
    }
}