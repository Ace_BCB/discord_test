module.exports = {
    name: 'ping',
    description: 'Pings the bot',
    cooldown: 5,
    execute(msg, args) {
        msg.reply('Pong!');
    }
}