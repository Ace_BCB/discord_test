module.exports = {
    name: 'boop',
    description: 'boops the first user mentioned"',
    usage: '<@user>',
    args: true,
    execute(msg, args) {
        const tagged_user = msg.mentions.users.first();

        if (!msg.mentions.users.size) {
            msg.reply(' you need to tag a user');
            return;
        }

        msg.channel.send('Boop ' + tagged_user.username);
    }
}