const fs = require('fs');
const {prefix, token} = require('./config.json');
const Discord = require('discord.js');

const client = new Discord.Client();
client.commands = new Discord.Collection();
const cooldowns = new Discord.Collection();

const bot_test_channel = "899609282931216455";

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
  const command = require(`./commands/${file}`);

  client.commands.set(command.name, command);
}

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
  let gonde = "185446982389596169";
  let me = "264123200290422795";
  if (msg.author.id === gonde) {
      msg.guild.channels.cache.get("899609282931216455").send("@everyone\nHe is alive\n" + (new Date()));
  }

  //if there is no prefix or is written by a bot then return or if it is written in a channel other than bot_test_channel
  if (!msg.content.startsWith(prefix) || msg.author.bot || msg.channel.id != bot_test_channel) return;

  //args is an array split be the spaces without the prefix
  const args = msg.content.slice(prefix.length).trim().split(/ +/);

  //command keyword and also removes command keyword from args so that is only arguments left
  const commandName = args.shift().toLowerCase();

  //return if there is no command with that name or alias
  const command = client.commands.get(commandName) || client.commands.find(c => c.aliases && c.aliases.includes(commandName));
  if (!command) return;

  //if there are no arguments on a cmd that needs it tell the user and return
  if (arguments_check(msg, command, args)) return;

  //check for cooldown on command and user
  cooldown_check(msg, command);

  //try to execute the cmd
  try {
    command.execute(msg, args);
  } catch (error) {
    console.error(error);
    msg.reply('there was an error trying to execute that command');
  }

})

client.on('typingStart', typing => {
    console.log("here");
    let gonde = "185446982389596169";
    let me = "264123200290422795";

    if (typing.member === me) {
        typing.channel.send("He is alive");
    }
});

client.on('presenceUpdate', (oldPresence, newPresence) => {
    console.log("here");
    let gonde = "185446982389596169";
    let me = "264123200290422795";
    let member = newPresence.member;
    // User id of the user you're tracking status.
    if (member.id === me) {
        if (oldPresence.status !== newPresence.status) {

            // Your specific channel to send a message in.
            let channel = member.guild.channels.cache.get("899609282931216455");
            // You can also use member.guild.channels.resolve('<channelId>');

            let text;

            if (newPresence.status === "online") {
                text = "He is alive!\n" + Date.now();
            } else if (newPresence.status === "offline") {
                text = "Oh no he died\n" + Date.now();
            }
            // etc...

            channel.send(text);
        }
    }
});

client.login(token);

function arguments_check(msg, command, args) {
  let res = false;

  if (command.args && !args.length) {
    let reply = "You didn't provide any arguments, " + msg.author + "!\n";

    if (command.usage) {
      reply += "The proper usage is:" + prefix + command.name + " " + command.usage;
    } else {
      reply += "Missing usage guide";
    }

    msg.channel.send(reply);
    res = true;
  }

  return res;
}

function cooldown_check(msg, command) {
  //if the command isn't in the collection add to the collection
  if (!cooldowns.has(command.name)) {
    cooldowns.set(command.name, new Discord.Collection());
  }

  const now = Date.now();
  const timestamps = cooldowns.get(command.name);
  const cooldownAmount = (command.cooldown || 3) * 1000;

  if (timestamps.has(msg.author.id)) {
    const expirationTime = timestamps.get(msg.author.id) + cooldownAmount;

    if (now < expirationTime) {
      const timeLeft = (expirationTime - now) / 1000;
      msg.reply(" please wait " + timeLeft + "s uttil you reuse the command");
    } else {
      timestamps.set(msg.author.id, now);
      setTimeout(() => timestamps.delete(msg.author.id), cooldownAmount);
    }
  }
}
